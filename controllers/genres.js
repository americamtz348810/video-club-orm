const express= require('express');
const { Genre } = require('../db');

function create (req, res, next) {
    const description = req.body.description;
    const status =req.body.status;

    genre.create({
        description: description,
        status: status
    }).then(object => res.json(object))
      .catch(err => res.send(err));
    };

function list (req, res, next) {
    genre.findAll()
        .then(object => res.json(object))
        .catch(err => res.send(err));
  }

function index (req, res, next) {
    const id = req.params.id;
    Genre.findByPk(id)
    .then(object => res.json(object))
    .catch(err => res.send(err));
}

function replace (req, res, next) {
    const id = req.params.id;
    Genre.findByPk(id)
        .then(object => {
            const description = request.body.description ? req.body.description : '';
            const status = request.body.status ? req.body.status : false;
            object.update({description : description, status : status})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
        })
        .catch(err => res.send(err));
}

function update(req, res, next) {
    const id = req.params.id;
    Genre.findByPk(id)
        .then(object => {
            const description = request.body.description ? req.body.description : object.description;
            const status = request.body.status ? req.body.status : object.status;
            object.update({description : description, status : status})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
        })
        .catch(err => res.send(err));
}

function destroy (req, res, next) {
    const id= req.params.id;
    Genre.destroy({where: {id : id}})
        .then(obj => res.json(obj))
        .catch(err => send(err));
}

module.exports={
    create, list, index, replace, update, destroy
};