const express= require('express');

const { Booking } = require('../db');

function create (req, res, next) {
    const date = req.body.date;
    const memberId = req.body.memberId;
    const copyId = req.body.copyId;
    
    Booking.create ({
        date : date,
        memberId : memberId,
        copyId : copyId
    }).then(object => res.json(object)).catch(err => res.send(err));
}

function list (req, res, next) {
    Movie.finfAll({include:['genre', 'director']})
        .then(objects => res.json(objects))
        .catch(err => res.send(err));
  }

function index (req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id)
        .then(object => res.json(object))
        .catch(err => res.send(err));
}

function replace (req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id)
        .then(object => {
            const date = req.body.date ? req.body.date : "";
            const memberId = req.body.memberId ? req.body.memberId: "";
            const copyId = req.body.fcopyId ? req.body.copyId: "";
            object.update({
                date : date,
                memberId : memberId,
                copyId :  copyId
            }).then(obj => res.json(obj))
              .catch(err => res.send(err));
        })
        .catch(err => res.send(err));
}

function update(req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id)
        .then(object => {
            const date = request.body.date ? req.body.date : object.date;
            const memberId = request.body.memberId ? req.body.memberId : object.memberId;
            const copyId = request.body.copyId ? req.body.copyId : object.copyId;
            object.update({
                date : date, 
                memberId : memberId,
                copyId : copyId
            })
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
        })
        .catch(err => res.send(err));
}

function destroy (req, res, next) {
    const id= req.params.id;
    Booking.destroy({where: {id : id}})
        .then(obj => res.json(obj))
        .catch(err => send(err));
}

module.exports={
    create, list, index, replace, update, destroy
};