const express= require('express');

const { Copy } = require('../db');

function create (req, res, next) {
    const number = req.body.number;
    const format = req.body.format;
    const movieId = req.body.movieId;
    const estaus = req.body.estaus;
    
    Copy.create ({
        number : number,
        format : format,
        movieId : movieId,
        estaus : estaus
    }).then(object => res.json(object)).catch(err => res.send(err));
}

function list (req, res, next) {
    Movie.finfAll({include:['movie']})
        .then(objects => res.json(objects))
        .catch(err => res.send(err));
  }

function index (req, res, next) {
    const id = req.params.id;
    Copy.findByPk(id)
        .then(object => res.json(object))
        .catch(err => res.send(err));
}

function replace (req, res, next) {
    const id = req.params.id;
    Copy.findByPk(id)
        .then(object => {
            const number = req.body.number ? req.body.number : "";
            const format = req.body.format ? req.body.format: "";
            const movieId = req.body.fmovieId ? req.body.movieId: "";
            const estaus = req.body.estaus ? req.body.estaus: "";
            object.update({
                number : number,
                format : format,
                movieId :  movieId,
                estaus: estaus
            }).then(obj => res.json(obj))
              .catch(err => res.send(err));
        })
        .catch(err => res.send(err));
}

function update(req, res, next) {
    const id = req.params.id;
    Copy.findByPk(id)
        .then(object => {
            const number = request.body.number ? req.body.number : object.number;
            const format = request.body.format ? req.body.format : object.format;
            const movieId = request.body.movieId ? req.body.movieId : object.movieId;
            const estaus = request.body.estaus ? req.body.estaus : object.estaus;
            object.update({
                number : number, 
                format : format,
                movieId : movieId,
                estaus : estaus
            })
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
        })
        .catch(err => res.send(err));
}

function destroy (req, res, next) {
    const id= req.params.id;
    Copy.destroy({where: {id : id}})
        .then(obj => res.json(obj))
        .catch(err => send(err));
}

module.exports={
    create, list, index, replace, update, destroy
};